package com.company;
public class Main {
    public static void main(String[] args) {
	// write your code here
        System.out.println( "-------- Min and max --------");
        int [] mas = {12,-4,8,123,78,56,22,-8,18,39,40};
        int max=0;
        int min=0;
        for (int i=1; i<11; i++) {
            System.out.print(mas[i] + " ");
        }
        System.out.println( " ");
        for (int i=1; i<11; i++){
            if(mas[i]>max){
                max=mas[i];
            }
            if (mas[i]<min){
                min=mas[i];
            }
        }
        System.out.println("max = "+max);
        System.out.println("min = "+min);
    }
}
